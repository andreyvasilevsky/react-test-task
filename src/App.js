import React, {Component} from 'react';
import './App.css';
import Layout from "./Hoc/Layout";
import Comments from "./Components/Comments";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Layout>
                    <Comments />
                </Layout>
            </div>
        );
    }
}

export default App;
