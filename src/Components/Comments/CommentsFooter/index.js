import React from 'react';
import './style.css'

const CommentsFooter = ({addComment, onChangeField} ) => {
    return (
        <div className={'comments-footer-container container'}>
            <input type="text" onChange={e => onChangeField(e)}/>
            <button className={'btn btn-primary send-btn'} placeholder={'Start typing your comment here'} onClick={addComment}>Submit</button>
        </div>
    )
}

export default CommentsFooter;