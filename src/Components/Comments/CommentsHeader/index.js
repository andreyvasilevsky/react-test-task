import React from 'react';
import './style.css'

const CommentsHeader = ({user, searchComments}) => {
    return (
        <div className={'comments-header container'}>
            <div className={'comments-header-container'}>
                <div className={'user-container'}>
                    <p>{user.name}</p>
                    <span>{user.status}</span>
                </div>
                <div className={'search-container'}>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">Search</span>
                        </div>
                        <input type="text" className="form-control" placeholder="Type something to search"
                               aria-describedby="basic-addon1" onChange={(event) => searchComments(event)}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CommentsHeader;