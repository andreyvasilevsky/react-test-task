import React, {Component} from 'react';
import './style.css'
import * as moment from 'moment'

class Comment extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        edit: false,
        editedComment: '',
        time: moment().format()
    };

    componentDidMount() {
        const {onTimePass, comment} = this.props;
        if (comment.editable) {
            window.setTimeout(() => {
                onTimePass(comment.id);
                this.handleEditState();
            }, 300000)
        }
    };

    onChangeField = e => {
        let {value} = e.target;
        this.setState({
            editedComment: value
        });
    };

    handleEditState = () => {
        this.setState((prevState) => ({
            edit: !prevState.edit
        }));
    };

    handleEditSaveEvent = () => {
        const {comment, editComment} = this.props;
        const {editedComment} = this.state;
        this.setState({
            edit: false
        });
        editComment(comment.id, editedComment)
    };

    showButtons = () => {
        const {comment} = this.props;
        const {edit} = this.state;
        if (comment.editable && !edit) {
            return (
                <div className={'edit-container'}>
                    <p>{comment.text}</p>
                    <button className={'btn btn-warning edit-btn'} onClick={() => this.handleEditState()}>Edit</button>
                </div>
            )
        } else if (comment.editable && edit) {
            return (
                <div className={'save-container'}>
                    <textarea type="text" onChange={this.onChangeField} defaultValue={comment.text}
                              className={'edit-field'}/>
                    <div className={'save-btn-container'}>
                        <button className={'btn btn-success save-btn'} onClick={this.handleEditSaveEvent}>Save</button>
                    </div>
                </div>
            )
        } else {
            return <p>{comment.text}</p>
        }
    };

    likeButtonHandler = () => {
        const { comment, likeComment } = this.props;
        likeComment(comment.id)
    };

    render() {
        const {comment} = this.props;
        return (
            <div className={'comment-container'}>
                <div className={'comment-name'}>
                    <p>{comment.name}</p>
                    <div className={'comment-date'}>{comment.date}</div>
                </div>
                {this.showButtons()}
                <div className={'likes-container'} onClick={this.likeButtonHandler}>
                    <h6>Likes <span className="badge badge-secondary">{comment.likes.length}</span></h6>
                </div>
            </div>
        )
    }
}


export default Comment