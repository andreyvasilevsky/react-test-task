import React, {Component} from 'react';
import Comment from './Comment/index'
import './style.css'

class CommentsMain extends Component {

    constructor(props) {
        super(props);
    }

    showComments = comments =>
        comments && comments.length
            ? comments.map((comment, index) => (
                <div key={index}>
                    <Comment comment={comment}
                             editComment={this.props.editComment}
                             onTimePass={this.props.onTimePass}
                             user={this.props.user}
                             likeComment={this.props.likeComment}
                    />
                </div>
            ))
            : null;

    render() {
        const { comments } = this.props;
        return (
            <div className={'container'}>
                {this.showComments(comments)}
            </div>
        )
    }
}

export default CommentsMain;