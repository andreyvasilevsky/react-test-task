import React, {Component} from 'react';
import CommentsFooter from './CommentsFooter/index'
import CommentsHeader from './CommentsHeader/index'
import CommentsMain from './CommentsMain/index'
import './style.css'
import * as moment from 'moment'

class Comments extends Component {
    state = {
        user: {
            name: 'Alex Mortinger',
            status: 'online'
        },
        comments: [
            {
                id: 1,
                name: 'Alex Mortinger',
                date: moment().format('MM/DD/YYYY, h:mm:ss a'),
                editable: false,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                likes: []
            },
            {
                id: 2,
                name: 'Alex Mortinger',
                date: moment().format('MM/DD/YYYY, h:mm:ss a'),
                editable: false,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                likes: []
            },
            {
                id: 3,
                name: 'Alex Mortinger',
                date: moment().format('MM/DD/YYYY, h:mm:ss a'),
                editable: false,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                likes: []
            },
            {
                id: 4,
                name: 'Alex Mortinger',
                date: moment().format('MM/DD/YYYY, h:mm:ss a'),
                editable: false,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                likes: []
            },
        ],
        newComment: {},
        initialComments: []
    };

    componentDidMount = () => {
        this.setState({
            initialComments: this.state.comments
        })
    };

    onChangeField = e => {
        const {value} = e.target;
        this.setState((prevState) => ({
            newComment: {
                id: [...prevState.comments].length + 1,
                name: prevState.user.name,
                text: value,
                date: moment().format('MM/DD/YYYY, h:mm:ss a'),
                editable: true,
                likes: []
            }
        }));
    };

    handleAddNewComment = () => {
        const {newComment} = this.state;
        if (newComment.text) {
            this.setState(prevState => ({
                comments: [...prevState.comments, newComment],
                initialComments: [...prevState.comments, newComment]
            }));
        }
    };

    editComment = (id, editedComment) => {
        const {comments} = this.state;
        const newArr = comments.map(comment => {
            return comment.id === id
                ? {...comment, text: editedComment, date: moment().format('MM/DD/YYYY, h:mm:ss a')}
                : comment
        });
        this.setState({
            comments: newArr
        });
    };

    likeComment = (id) => {
        const {comments, user} = this.state;
        const newArr = comments.map(comment => {
            if (comment.id === id && comment.likes.indexOf(user.name) === -1) {
                comment.likes.push(user.name)
            } else if (comment.id === id) {
                const index = comment.likes.indexOf(user.name);
                comment.likes.splice(index, 1);
            }
            return comment;
        });
        this.setState({
            comments: newArr
        });
    };

    handleEditComment = id => {
        const {comments} = this.state;
        let newArr = comments.splice('');
        newArr = newArr.map(comment => {
            return comment.id === id
                ? {...comment, editable: false}
                : comment
        });
        this.setState({
            comments: newArr
        })
    };

    searchComments = event => {
        const { value } = event.target;
        let newArr = this.state.initialComments.filter(comment => {
            return comment.text.includes(value) ? comment : null
        });
        this.setState({
            comments: newArr
        })

    };

    render() {
        const {user, comments} = this.state;
        return (
            <div className={'comments-container container'}>
                <CommentsHeader user={user} searchComments={this.searchComments}/>
                <CommentsMain comments={comments} editComment={this.editComment}
                              onTimePass={this.handleEditComment} user={user} likeComment={this.likeComment}/>
                <CommentsFooter addComment={this.handleAddNewComment} onChangeField={this.onChangeField}/>
            </div>
        )
    }
}

export default Comments