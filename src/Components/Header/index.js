import React from 'react'
import './style.css'

const Header = () => {
    return (
        <div>
            <div className={'bg-primary'}>
                <div className={'header-container container'}>
                    <div className={'left-menu'}>
                        <i className="fas fa-users active"></i>
                        <i className="fas fa-bars"></i>
                    </div>
                    <div className={'right-menu'}>
                        <i className="fas fa-calendar-alt"></i>
                        <i className="far fa-envelope"></i>
                    </div>
                </div>
            </div>
            <div className={'sub-header'}>
            </div>
        </div>
    )
}

export default Header;