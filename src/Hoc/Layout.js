import React from 'react';
import Header from "../Components/Header/index";

const Layout = props => {
    return (
        <div>
            <Header/>
            {props.children}
        </div>
    )
}

export default Layout;